#!/usr/bin/env fennel

(local door
	   {:isOpen false}
	   )
(fn door.showState []
  "prints the state of the door"
  (if door.isOpen
	  (print "door is open")
	  (print "door is closed")
	  )
  )

(fn door.open []
  "opens door"
  (tset door :isOpen true)
  (door.showState)
  )

(fn door.close []
  "closes door"
  (tset door :isOpen false)
  (door.showState)
  )


(fn door.toggle []
  "toggles door"
  (tset door :isOpen (not door.isOpen))
  (door.showState)
  )

door
