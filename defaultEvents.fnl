#!/usr/bin/env fennel
(local door (require :door-dummy))
(local events {})

(fn events.onClick []
  (door.toggle)
  )
(fn events.attacked []
  (print "does nothing yet")
  )

(fn events.redstone [state]
  (local rs state.redstone)
  (local strength rs.strength)
  (if (= strength 0)
	(door.close)
	(>= strength 1)
	(door.open)
	)
  )

events
