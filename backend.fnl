#!/usr/bin/env fennel
(local events (require :usercode))

(fn event []
  "demo only-emulation of event-trigger loop"
  (local names
		 [
		  "onClick"
		  "attacked"
		  "redstone"
		  ]
		 )
  (each [i v (ipairs names)]
	(print (.. i ": " v))
	)
  (print (.. "pick an event(number between 1 and " (# names) " )>"))
  (. names (io.read "*n"))
  )

(fn main [args]
  "main function"
  (for [i 1 100]
	(print i)
	(local ev (event))
	(var states
		 [
		  {:player {:username "Acuadragon100"} :redstone {:side "east" :strength 0}}
		  {:player {:username "erilun06"} :redstone {:side "north" :strength 16}}
		  {:player {:username "herobrine"} :redstone {:side "west" :strength 0}}
		  ]
		 )
	(math.randomseed (os.time))
	(local state (. states (math.random 1 (# states))))
	(tset state :rs state.redstone)
	(print (table.concat ["player:" state.player.username] " "))
	((. events ev) state)
	)
  )



(main)
